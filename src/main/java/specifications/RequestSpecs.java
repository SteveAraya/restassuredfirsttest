package specifications;

import helpers.RequestHelper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class RequestSpecs {

    public static RequestSpecification generateToken(){
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        String token = RequestHelper.getUserToken();

        requestSpecBuilder.addHeader("Authorization", "Bearer " + token);
        return requestSpecBuilder.build();
    };

    public static RequestSpecification generateTokenFake(){
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdXVpZCI6IjRjYWVjYTM1LTQ1ZjAtNGZmNC04NDgzLTRlOGZlZWIxMTA2MCIsImF1dGhvcml6ZWQiOnRydWUsImV4cCI6MTYxMzg3Nzg2MywidXNlcl9pZCI6MjM5fQ.NF4FmDf2KEQ4pb0rF1ahJ9TSC-McZzkBVPvKMt90j61";

        requestSpecBuilder.addHeader("Authorization", "Bearer " + token);
        return requestSpecBuilder.build();
    };

}

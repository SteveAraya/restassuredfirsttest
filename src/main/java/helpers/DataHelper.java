package helpers;

import model.User;
import java.util.Random;

public class DataHelper {

    public static Integer createdPostID        = 0;
    public static Integer invalidPostID        = 000000;
    public static Integer createdPostIDComment = 12;
    public static Integer createdCommentID     = 0;
    public static Integer invalidCommentID     = 000000;

    public static String titlePostUpdate       = "Actualizar el post";
    public static String bodyPostUpdate        = "Contenido del post actualizado...";
    public static String titleCommentUpdate    = "Actualizar el comment";
    public static String bodyCommentUpdate     = "Contenido del comment actualizado...";

    public static String generateRandomTitle(){
        return String.format("%s" , generateRandomString(20));
    }

    public static String generateRandomContent(){
        return String.format("%s" , generateRandomString(100));
    }

    private static String generateRandomString(int targetStringLength){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();

        return generatedString;
    }

    public static User getTestUser(){
        return new User("Steve","steve@gmail.com", "steve12345");
    }

    public static String userBasicAuth(){
        return "testuser";
    }

    public static String passwordBasicAuth(){
        return "testpass";
    }

    public static String passwordBasicAuthFake(){
        return "testpassInvalid";
    }

}

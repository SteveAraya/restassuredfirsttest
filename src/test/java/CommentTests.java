import helpers.DataHelper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import model.Comment;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import specifications.ResponseSpecs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class CommentTests extends BaseTest{

    private static String resourcePath = "/v1/comment";

    @BeforeGroups("create_comment")
    public void createComment(){

        Comment testComment = new Comment(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        Response response = given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .body(testComment)
                .post(resourcePath + "/" + DataHelper.createdPostIDComment);

        JsonPath jsonPathEvaluator = response.jsonPath();
        DataHelper.createdCommentID = jsonPathEvaluator.get("id");

    }

    //////////////////////////////////////////
    //    Comment Test Create
    //////////////////////////////////////////
    @Test
    public void ATest_Create_Comment_Success(){

        Comment testComment = new Comment(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .body(testComment)
                .post(resourcePath + "/" + DataHelper.createdPostIDComment)
            .then()
                .body("message", equalTo("Comment created"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void ATest_Create_Comment_Invalid(){

        Comment testComment = new Comment(null, DataHelper.generateRandomContent());

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .body(testComment)
                .post(resourcePath  + "/" + DataHelper.createdPostIDComment)
            .then()
                .body("message", equalTo("Invalid form"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void ATest_Create_Comment_Security(){

        Comment testComment = new Comment(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuthFake())
                .body(testComment)
                .post(resourcePath  + "/" + DataHelper.createdPostIDComment)
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Comment Test Get All
    //////////////////////////////////////////
    @Test
    public void BTest_Get_All_Comment_Success(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .get(resourcePath + "s" + "/" + DataHelper.createdPostIDComment)
            .then()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void BTest_Get_All_Comment_Invalid(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .get(resourcePath + "s" + "/" )
            .then()
                .statusCode(404);
    }

    @Test
    public void BTest_Get_All_Comment_Security(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuthFake())
                .get(resourcePath + "s" + "/" + DataHelper.createdPostIDComment)
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //    Comment Test Get All
    @Test(groups = "create_comment")
    public void CTest_Get_One_Comment_Success(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .get(resourcePath + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString() )
            .then()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_comment")
    public void CTest_Get_One_Comment_Invalid(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .get(resourcePath + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.invalidCommentID.toString() )
            .then()
                .body("Message", equalTo("Comment not found"))
            .and()
                .statusCode(404)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_comment")
    public void CTest_Get_One_Comment_Security(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuthFake())
                .get(resourcePath + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString() )
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Comment Test Update
    //////////////////////////////////////////
    @Test
    public void DTest_Update_Comment_Success(){

        Comment testComment = new Comment(DataHelper.titleCommentUpdate, DataHelper.bodyCommentUpdate);

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .body(testComment)
                .put(resourcePath + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString())
            .then()
                .body("message", equalTo("Comment updated"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void DTest_Update_Comment_Invalid(){

        Comment testComment = new Comment(DataHelper.titleCommentUpdate, null);

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .body(testComment)
                .put(resourcePath  + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString())
            .then()
                .body("message", equalTo("Invalid form"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void DTest_Update_Comment_Security(){

        Comment testComment = new Comment(DataHelper.titleCommentUpdate, null);

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuthFake())
                .body(testComment)
                .put(resourcePath  + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString())
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Comment Test Delete
    //////////////////////////////////////////
    @Test(groups = "create_comment")
    public void FTest_Delete_Comment_Success(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .delete(resourcePath  + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString() )
            .then()
                .body("message", equalTo("Comment deleted"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_comment")
    public void FTest_Delete_Comment_Invalid(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuth())
                .delete(resourcePath  + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.invalidCommentID.toString() )
            .then()
                .body("message", equalTo("Comment could not be deleted"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_comment")
    public void FTest_Delete_Comment_Security(){

        given()
                .auth().basic(DataHelper.userBasicAuth(), DataHelper.passwordBasicAuthFake())
                .delete(resourcePath  + "/" + DataHelper.createdPostIDComment + "/" + DataHelper.createdCommentID.toString() )
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

}

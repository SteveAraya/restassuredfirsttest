import helpers.DataHelper;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import model.Post;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import specifications.RequestSpecs;
import specifications.ResponseSpecs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class PostTests extends BaseTest{

    private static String resourcePath = "/v1/post";

    @BeforeGroups("create_post")
    public void createPost(){

        Post testPost = new Post(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        Response response = given()
                .spec(RequestSpecs.generateToken())
                .body(testPost)
                .post(resourcePath);

        JsonPath jsonPathEvaluator = response.jsonPath();
        DataHelper.createdPostID = jsonPathEvaluator.get("id");

    }

    //////////////////////////////////////////
    //    Post Test Create
    //////////////////////////////////////////
    @Test
    public void ATest_Create_Post_Success(){

        Post testPost = new Post(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        given()
                .spec(RequestSpecs.generateToken())
                .body(testPost)
                .post(resourcePath)
            .then()
                .body("message", equalTo("Post created"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void ATest_Create_Post_Invalid(){

        Post testPost = new Post(null, DataHelper.generateRandomContent());

        given()
                .spec(RequestSpecs.generateToken())
                .body(testPost)
                .post(resourcePath)
            .then()
                .body("message", equalTo("Invalid form"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void ATest_Create_Post_Security(){

        Post testPost = new Post(DataHelper.generateRandomTitle(), DataHelper.generateRandomContent());

        given()
                .spec(RequestSpecs.generateTokenFake())
                .body(testPost)
                .post(resourcePath)
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Post Test Get All
    //////////////////////////////////////////
    @Test
    public void BTest_Get_All_Post_Success(){

        given()
                .spec(RequestSpecs.generateToken())
                .get(resourcePath + "s")
            .then()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void BTest_Get_All_Post_Invalid(){

        given()
                .spec(RequestSpecs.generateToken())
                .get(resourcePath)
            .then()
                .statusCode(404);
    }

    @Test
    public void BTest_Get_All_Post_Security(){

        given()
                .spec(RequestSpecs.generateTokenFake())
                .get(resourcePath + "s")
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Post Test Get One
    //////////////////////////////////////////
    @Test(groups = "create_post")
    public void CTest_Get_One_Post_Success(){

        given()
                .spec(RequestSpecs.generateToken())
                .get(resourcePath + "/" + DataHelper.createdPostID.toString() )
            .then()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_post")
    public void CTest_Get_One_Post_Invalid(){

        given()
                .spec(RequestSpecs.generateToken())
                .get(resourcePath + "/" + DataHelper.invalidPostID.toString() )
            .then()
                .body("Message", equalTo("Post not found"))
            .and()
                .statusCode(404)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_post")
    public void CTest_Get_One_Post_Security(){

        given()
                .spec(RequestSpecs.generateTokenFake())
                .get(resourcePath + "/" + DataHelper.createdPostID.toString() )
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Post Test Update
    //////////////////////////////////////////
    @Test
    public void DTest_Update_Post_Success(){

        Post testPost = new Post(DataHelper.titlePostUpdate, DataHelper.bodyPostUpdate);

        given()
                .spec(RequestSpecs.generateToken())
                .body(testPost)
                .put(resourcePath + "/" + DataHelper.createdPostID.toString())
            .then()
                .body("message", equalTo("Post updated"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void DTest_Update_Post_Invalid(){

        Post testPost = new Post(DataHelper.titlePostUpdate, null);

        given()
                .spec(RequestSpecs.generateToken())
                .body(testPost)
                .put(resourcePath + "/" + DataHelper.createdPostID.toString())
            .then()
                .body("message", equalTo("Invalid form"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test
    public void DTest_Update_Post_Security(){

        Post testPost = new Post(DataHelper.titlePostUpdate, DataHelper.bodyPostUpdate);

        given()
                .spec(RequestSpecs.generateTokenFake())
                .body(testPost)
                .put(resourcePath + "/" + DataHelper.createdPostID.toString())
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

    //////////////////////////////////////////
    //    Post Test Delete
    //////////////////////////////////////////
    @Test(groups = "create_post")
    public void FTest_Delete_Post_Success(){

        given()
                .spec(RequestSpecs.generateToken())
                .delete(resourcePath + "/" + DataHelper.createdPostID.toString() )
            .then()
                .body("message", equalTo("Post deleted"))
            .and()
                .statusCode(200)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_post")
    public void FTest_Delete_Post_Invalid(){

        given()
                .spec(RequestSpecs.generateToken())
                .delete(resourcePath + "/" + DataHelper.invalidPostID.toString() )
            .then()
                .body("message", equalTo("Post could not be deleted"))
            .and()
                .statusCode(406)
                .spec(ResponseSpecs.defaultSpec());
    }

    @Test(groups = "create_post")
    public void FTest_Delete_Post_Security(){

        given()
                .spec(RequestSpecs.generateTokenFake())
                .delete(resourcePath + "/" + DataHelper.createdPostID.toString() )
            .then()
                .body("message", equalTo("Please login first"))
            .and()
                .statusCode(401)
                .spec(ResponseSpecs.defaultSpec());
    }

}
